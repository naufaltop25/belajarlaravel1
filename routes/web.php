<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');
Route::get('/register', 'AuthController@bio');
Route::post('/welcome', 'AuthController@kirim');
Route::get('/data-table', function() {
    return view('table.datatable');
});

// CRUD cast

Route::get('/cast/create', 'CastController@create'); // create
Route::post('/cast', 'CastController@store'); // store

Route::get('/cast', 'CastController@index'); // route menuju list cast
Route::get('/cast/{cast_id}', 'CastController@show'); // route detail cast

Route::get('/cast/{cast_id}/edit', 'CastController@edit'); // route menuju edit cast
Route::put('/cast/{cast_id}', 'CastController@update'); // route update cast

Route::delete('/cast/{cast_id}', 'CastController@destroy'); // route delete cast

