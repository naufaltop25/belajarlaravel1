@extends('layout.master')

@section('judul')
    Halaman Form
@endsection
@section('content')
    <h1>Buat Akun Baru</h1>
    <h3>Sign Up Form</h3><br>

    <form action="/welcome" method="post">
        @csrf
        <label for="firstname">First name :</label><br>
        <input type="text" name="firstname" id="firstname"><br><br>
        <label for="lastname">Last name :</label><br><br>
        <input type="text" name="lastname" id="lastname"><br><br>

        <label for="gender">Gender</label><br>
        <input type="radio" name="gender" id="gender">Male<br>
        <input type="radio" name="gender" id="gender">Female<br><br>

        <label for="nationality">Nationality</label><br>
        <select name="nationality" id="nationality">
            <option value="indonesia">Indonesia</option>
            <option value="inggris">Inggris</option>
            <option value="other">Other</option>
        </select><br><br>
        
        <label for="languange">Languange Spoken</label><br>
        <input type="checkbox" name="languange" id="languange">Bahasa Indonesia<br>
        <input type="checkbox" name="languange" id="languange">English<br>
        <input type="checkbox" name="languange" id="languange">Other<br><br>

        <label for="bio">Bio</label><br>
        <textarea name="bio" id="bio" cols="30" rows="10"></textarea><br>

        <button type="submit">Submit</button>
    </form>
@endsection

