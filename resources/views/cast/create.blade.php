@extends('layout.master')

@section('judul')
    Tambah Data Pemain Film
@endsection

@section('content')
<form action="/cast" method="POST">
    @csrf
    <div class="form-group">
        <label for="nama">Nama</label>
        <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukan nama" autocomplete="off">
    </div>
    @error('nama')
        <div class="alert alert-danger alert-dismissible fade show" role="alert">{{ $message }}
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
    @enderror
    <div class="form-group">
        <label for="umur">Umur</label>
        <input type="text" class="form-control" name="umur" id="umur" placeholder="Masukkan umur" autocomplete="off">
    </div>
    @error('umur')
        <div class="alert alert-danger alert-dismissible fade show" role="alert">{{ $message }}
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
    @enderror
    <div class="form-group">
        <label for="bio">Biodata</label>
        <textarea class="form-control" name="bio" id="bio" placeholder="Masukkan biodata" rows="3"></textarea>
      </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
