@extends('layout.master')

@section('judul')
    List Pemain Film
@endsection

@section('content')

<a href="/cast/create" class="btn btn-secondary mb-3">Tambah Data Pemain Film</a>

<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Biodata</th>
        <th scope="col">Tindakan</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key => $item)
          <tr>
            <th scope="row">{{$key + 1}}</th>
            <td>{{$item->nama}}</td>
            <td>{{$item->umur}} th</td>
            <td class="col-6">{{$item->bio}}</td>
            <td>
                <form action="/cast/{{$item->id}}" method="POST">
                    <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                    <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                    @csrf
                    @method('delete')
                    <button type="submit" name="submit" class="btn btn-danger btn-sm" onclick="return confirm('Yakin?');">Delete</a>
                </form>
            </td>
          </tr>
        @empty
            <h1>Data tidak ada</h1>
        @endforelse
    </tbody>
  </table>  
@endsection
