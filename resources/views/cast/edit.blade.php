@extends('layout.master')

@section('judul')
    Edit Data Pemain Film
@endsection

@section('content')
<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label for="nama">Nama</label>
        <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukan nama" autocomplete="off" value="{{$cast->nama}}">
    </div>
    <div class="form-group">
        <label for="umur">Umur</label>
        <input type="text" class="form-control" name="umur" id="umur" placeholder="Masukkan umur" value="{{$cast->umur}}" autocomplete="off">
    </div>
    <div class="form-group">
        <label for="bio">Biodata</label>
        <textarea class="form-control" name="bio" id="bio" placeholder="Masukkan biodata" rows="3">{{$cast->bio}}</textarea>
      </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
