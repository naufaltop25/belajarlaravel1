@extends('layout.master')

@section('judul')
    Detail Pemain Film
@endsection

@section('content')

<h1>{{$cast->nama}}</h1>
<p>Umur : {{$cast->umur}}th</p>
<p>{{$cast->bio}}</p>

@endsection
